"""concierge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin, auth
from django.conf import settings
from django.conf.urls.static import static
import django.contrib.auth.views
from django.utils.translation import ugettext as _
from django.views.generic import TemplateView

from engine.forms import CustomLoginForm, CustomRegForm
from engine.views import RegPage
from django.conf.urls.i18n import i18n_patterns

urlpatterns = i18n_patterns(
    url(r'^admin/', admin.site.urls),
    url(r'^', include('engine.urls')),
    url(r'^login/$',
        auth.views.login,
        {
            'authentication_form': CustomLoginForm,
            'extra_context':
                {
                    'title': _('Sign in'),
                }
        },
        name='login'),
    url(r'^logout/$', auth.views.logout, name='logout'),
    url(r'^register/$', RegPage.as_view(), name='registration_register'),
    url(r'^d10731ac8ae1.html/$', TemplateView.as_view(template_name="engine/pages/d10731ac8ae1.html"), name='yandex'),
    url(r'ckeditor/', include('ckeditor_uploader.urls')),
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
