from modeltranslation.translator import translator, TranslationOptions
from .models import Country, City


class CountryTranslationOptions(TranslationOptions):
    fields = ('name', 'full_desc',)


class CityTranslationOptions(TranslationOptions):
    fields = ('name', 'full_desc',)


translator.register(Country, CountryTranslationOptions)
translator.register(City, CityTranslationOptions)
