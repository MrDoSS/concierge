from django.views.generic import ListView
from engine.models import CatService
from geography.models import Country, City


class CitiesView(ListView):
    model = City
    template_name = "engine/cities.html"
    country = None
    context_object_name = 'city_or_state_list'

    def dispatch(self, request, *args, **kwargs):
        self.country = Country.objects.filter(slug=self.kwargs.get('country')).first()
        return super(CitiesView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = City.objects.filter(country=self.country).order_by('name')
        return qs

    def get_context_data(self, **kwargs):
        context = super(CitiesView, self).get_context_data(**kwargs)
        context["title"] = self.country.name
        context['banner_img'] = self.country.image.url
        context['description'] = self.country.full_desc
        return context