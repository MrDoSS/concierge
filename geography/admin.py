from django.contrib import admin
from .models import Country, City
from modeltranslation.admin import TranslationAdmin


@admin.register(Country)
class CountryAdmin(TranslationAdmin):
    prepopulated_fields = {'slug': ('name', )}
    list_display = ['name', 'slug']

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


@admin.register(City)
class CityAdmin(TranslationAdmin):
    prepopulated_fields = {'slug': ('name', )}
    list_display = ['name', 'slug']

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }