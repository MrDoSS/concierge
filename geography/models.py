from django.db import models
from django.urls import reverse
from django_resized import ResizedImageField
from django.utils.translation import ugettext as _
from ckeditor_uploader.fields import RichTextUploadingField


class Country(models.Model):
    name = models.CharField(max_length=50, unique=True)
    slug = models.SlugField()
    image = ResizedImageField(upload_to='images/countries/')
    full_desc = RichTextUploadingField(verbose_name=_("Full description"), blank=True, null=True)
    order = models.PositiveSmallIntegerField(verbose_name=_("Output order"), default=1)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cities_list', kwargs={'country': self.slug})

    class Meta:
        verbose_name_plural = 'Countries'


class City(models.Model):
    name = models.CharField(max_length=50, unique=True)
    country = models.ForeignKey(Country, null=True, blank=True)
    slug = models.SlugField()
    image = ResizedImageField(upload_to='images/cities/')
    full_desc = RichTextUploadingField(verbose_name=_("Full description"), blank=True, null=True)


    def __str__(self):
        return self.name

    def get_absolute_url(self):
        country = self.country.slug
        return reverse('cats_list', kwargs={'city': self.slug, 'country': country})

    class Meta:
        verbose_name_plural = 'Cities'


