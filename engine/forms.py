from django import forms
from geography.models import City
from .models import Application, User, Service, CatService, Variation
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.utils.translation import ugettext as _


class CreateApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        fields = '__all__'
        exclude = ['user', 'service', 'status', 'city']
        widgets = {
            'check_in': forms.DateInput(attrs={'type': 'date'}),
            'check_out': forms.DateInput(attrs={'type': 'date'}),
        }

    def __init__(self, variations, *args, **kwargs):
        super(CreateApplicationForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].label = _(self.fields[field].label)
        if variations:
            self.fields['variation'] = forms.ModelChoiceField(
                queryset=variations)
            self.fields['variation'].widget.attrs['class'] = 'form-control'
        else:
            self.fields.pop('variation')


class CustomRegForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'phone', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super(CustomRegForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].label = _(self.fields[field].label)
        self.fields['username'].widget.attrs.pop('autofocus')


class CustomLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].label = _(self.fields[field].label)
        self.fields['username'].widget.attrs.pop('autofocus')


class AccountEditForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone',]

    def __init__(self, *args, **kwargs):
        super(AccountEditForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'
            self.fields[field].label = _(self.fields[field].label)
