from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline
from .models import CatService, Service, Application, User, Gallery, Variation

class GalleryInlines(GenericStackedInline):
    model = Gallery
    exclude = ()
    extra = 1


class VariationInline(TranslationStackedInline):
    model = Variation
    extra = 0


@admin.register(CatService)
class CatAdmin(TranslationAdmin):
    prepopulated_fields = {'slug': ('name', )}
    list_display = ['name', 'slug']

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


@admin.register(Service)
class ServiceAdmin(TranslationAdmin):
    prepopulated_fields = {'slug': ('name', )}
    list_display = ['name', 'price']
    inlines = (VariationInline, GalleryInlines)

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


@admin.register(Application)
class ApplicationAdmin(admin.ModelAdmin):
    list_display = ['user', 'service', 'created']


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name', 'phone']

