from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, DetailView, ListView, CreateView, UpdateView

from configs import models as config
from engine.forms import CreateApplicationForm, AccountEditForm, CustomRegForm
from django.utils.translation import ugettext as _
from engine.models import User, Gallery, Variation, Feature
from geography.models import Country, City
from .models import CatService, Service, Application
from concierge import settings


class MainPage(TemplateView):
    template_name = "engine/pages/index.html"
    config = config.MainPageConfig.objects.get()

    def get_context_data(self, **kwargs):
        context = super(MainPage, self).get_context_data(**kwargs)
        context["services"] = Country.objects.all().order_by("order")
        context["features"] = Feature.objects.all().order_by("id")
        context["title"] = self.config.title
        context[
            'description'] = self.config.desc
        context['banner_img'] = settings.STATIC_URL + 'site/images/frontpage-banner.jpg'
        return context


class CatsView(ListView):
    model = CatService
    template_name = "engine/cats.html"
    city = None

    def dispatch(self, request, *args, **kwargs):
        self.city = City.objects.filter(slug=self.kwargs.get('city')).first()
        request.session['city'] = self.city.slug
        return super(CatsView, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = CatService.objects.filter(cities=self.city)
        return qs

    def get_context_data(self, **kwargs):
        context = super(CatsView, self).get_context_data(**kwargs)
        context["title"] = self.city.name
        context['banner_img'] = self.city.image.url
        context['description'] = self.city.full_desc
        return context


class ServicesFromCatList(ListView):
    model = Service
    template_name = "engine/cat_service_list.html"
    cat = None
    city = None

    def dispatch(self, request, *args, **kwargs):
        self.cat = CatService.objects.filter(slug=self.kwargs.get('cat')).first()
        self.city = City.objects.filter(slug=request.session['city']).first()
        return super(ServicesFromCatList, self).dispatch(request, *args, **kwargs)

    def get_queryset(self):
        qs = Service.objects.filter(category=self.cat, cities=self.city)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ServicesFromCatList, self).get_context_data(**kwargs)
        context["title"] = self.cat.name
        context['description'] = self.cat.desc
        context['banner_img'] = self.cat.image.url
        return context


class AllServices(ListView):
    model = Country
    template_name = "engine/all_services.html"

    def get_context_data(self, **kwargs):
        context = super(AllServices, self).get_context_data(**kwargs)
        context["title"] = _("Services")
        return context


class ServiceDetail(DetailView, CreateView):
    model = Service
    form_class = CreateApplicationForm
    template_name = 'engine/service_detail.html'
    success_url = '/success'
    variations = None

    def dispatch(self, request, *args, **kwargs):
        self.variations = Variation.objects.filter(service=self.get_object().pk)
        return super(ServiceDetail, self).dispatch(request, *args, **kwargs)

    def get_form(self, form_class=None):
        if form_class is None:
            form_class = self.get_form_class()
        return form_class(self.variations, **self.get_form_kwargs())

    def get_context_data(self, **kwargs):
        context = super(ServiceDetail, self).get_context_data(**kwargs)
        context["title"] = self.object.name
        context['description'] = self.object.desc
        context['banner_img'] = self.object.image.url
        context['gallery'] = Gallery.objects.gallery_for_object(self.get_object())
        context['variations'] = self.variations
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.service = self.get_object()
        form.instance.city = City.objects.filter(slug=self.request.session['city']).first()
        return super(ServiceDetail, self).form_valid(form)


class AboutPage(TemplateView):
    template_name = "engine/pages/about-us.html"
    config = config.AboutUsPageConfig.objects.get()

    def get_context_data(self, **kwargs):
        context = super(AboutPage, self).get_context_data(**kwargs)
        context["title"] = self.config.title
        context[
            'description'] = self.config.desc
        return context


class ContactsPage(TemplateView):
    template_name = "engine/pages/contacts.html"

    def get_context_data(self, **kwargs):
        context = super(ContactsPage, self).get_context_data(**kwargs)
        context["title"] = _("Contacts")
        return context


class SuccessPaymentPage(TemplateView):
    template_name = "engine/pages/success.html"

    def get_context_data(self, **kwargs):
        context = super(SuccessPaymentPage, self).get_context_data(**kwargs)
        context["title"] = _("Success")
        return context


# class RegPage(RegistrationView):
#     def get_context_data(self, **kwargs):
#         context = super(RegPage, self).get_context_data(**kwargs)
#         context['title'] = 'Registration'
#         return context


class RegPage(CreateView):
    model = User
    form_class = CustomRegForm
    template_name = 'registration/registration_form.html'
    success_url = '/login'

    def get_context_data(self, **kwargs):
        context = super(RegPage, self).get_context_data(**kwargs)
        context['title'] = _('Registration')
        return context


class AccountView(UpdateView):
    template_name = "engine/pages/account.html"
    model = User
    form_class = AccountEditForm
    success_url = "."

    @method_decorator(login_required())
    def dispatch(self, request, *args, **kwargs):
        return super(AccountView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AccountView, self).get_context_data(**kwargs)
        context["title"] = _("Account")
        context["applications"] = Application.objects.filter(user=self.get_object())
        return context

    def get_object(self, queryset=None):
        return auth.get_user(self.request)
