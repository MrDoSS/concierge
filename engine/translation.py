from modeltranslation.translator import translator, TranslationOptions
from .models import Service, CatService, Variation, Feature


class ServiceTranslationOptions(TranslationOptions):
    fields = ('name', 'desc', 'full_desc')


class CatServiceTranslationOptions(TranslationOptions):
    fields = ('name', 'desc')


class VariationTranslationOptions(TranslationOptions):
    fields = ('name',)


class FeatureTranslationOptions(TranslationOptions):
    fields = ('title', 'desc')


translator.register(Service, ServiceTranslationOptions)
translator.register(CatService, CatServiceTranslationOptions)
translator.register(Variation, VariationTranslationOptions)
translator.register(Feature, FeatureTranslationOptions)

