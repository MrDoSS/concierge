from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext as _
from django.db import models
from django.urls import reverse
from django_resized import ResizedImageField

from configs.models import MainPageConfig
from geography.models import City
from ckeditor_uploader.fields import RichTextUploadingField


class User(AbstractUser):
    phone = models.CharField(max_length=15, verbose_name=_("Phone"), blank=True, null=True)


class CatService(models.Model):
    name = models.CharField(max_length=1000, verbose_name=_("Name"), unique=True)
    desc = RichTextUploadingField(verbose_name=_("Description"))
    slug = models.SlugField(max_length=1000, verbose_name=_("Slug"))
    image = ResizedImageField(upload_to='images/cat/%Y/', verbose_name=_("Image"))
    cities = models.ManyToManyField(City, verbose_name=_("City"))

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('cat_service_list', kwargs={'cat': self.slug})

    def get_services(self):
        return Service.objects.filter(category=self)


class Service(models.Model):
    category = models.ForeignKey(CatService, verbose_name=_("Category"))
    name = models.CharField(max_length=1000, verbose_name=_("Name"), unique=True)
    desc = RichTextUploadingField(verbose_name=_("Short description"))
    full_desc = RichTextUploadingField(verbose_name=_("Full description"), blank=True, null=True)
    slug = models.SlugField(max_length=1000, verbose_name=_("Slug"))
    image = ResizedImageField(upload_to='images/service/%Y/', verbose_name=_("Image"))
    price = models.PositiveSmallIntegerField(verbose_name=_("Price"))
    cities = models.ManyToManyField(City, verbose_name=_("City"))
    order = models.PositiveSmallIntegerField(verbose_name=_("Output order"), default=1)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('service_detail', kwargs={'slug': self.slug})


class Variation(models.Model):
    service = models.ForeignKey(Service, verbose_name=_("Service"))
    name = models.CharField(max_length=1000, verbose_name=_("Name"))
    price = models.PositiveSmallIntegerField(verbose_name=_("Price"))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('variation')
        verbose_name_plural = _("variations")


class Application(models.Model):
    class STATUS:
        ACCEPT = _('Application processed')
        DENY = _('Application denied')
        WAIT = _('Application on moderation')

        CHOICES = (
            (ACCEPT, _('Application processed')),
            (DENY, _('Application denied')),
            (WAIT, _('Application on moderation')),
        )

    user = models.ForeignKey(User, verbose_name=_("User"))
    service = models.ForeignKey(Service, verbose_name=_("Service"))
    check_in = models.DateField(verbose_name=_("Check in"), blank=True, null=True)
    check_out = models.DateField(verbose_name=_("Check out"), blank=True, null=True)
    adults = models.PositiveSmallIntegerField(verbose_name=_("Adults"), blank=True, null=True)
    kids = models.PositiveSmallIntegerField(verbose_name=_("Kids"), blank=True, null=True)
    price_from = models.PositiveSmallIntegerField(verbose_name=_("Price from"))
    price_to = models.PositiveSmallIntegerField(verbose_name=_("Price to"))
    status = models.CharField(max_length=1000, choices=STATUS.CHOICES, default=STATUS.WAIT, verbose_name=_("Status"))
    city = models.ForeignKey(City, verbose_name=_("City"))
    variation = models.ForeignKey(Variation, verbose_name=_("Variation"), null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)


class GalleryManager(models.Manager):
    def gallery_for_object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        return self.filter(content_type__pk=object_type.id,
                           object_id=obj.pk)


class Gallery(models.Model):
    name = models.CharField(max_length=1000, verbose_name=_("Name"))
    image = ResizedImageField(verbose_name=_("Image"), upload_to='images/galleries/')
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    objects = GalleryManager()

    class Meta:
        verbose_name = 'image'
        verbose_name_plural = "Gallery"

    def __str__(self):
        return self.name


class Feature(models.Model):
    title = models.CharField(max_length=255, verbose_name=_("Title"))
    desc = models.TextField(verbose_name=_("Description"))
    main_page = models.ForeignKey(MainPageConfig, on_delete=models.CASCADE)

    def __str__(self):
        return self.title