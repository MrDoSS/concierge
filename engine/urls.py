from django.conf.urls import url
from . import views
from geography.views import CitiesView

urlpatterns = [
    url(r'^$', views.MainPage.as_view(), name='main_page'),
    url(r'^services/$', views.AllServices.as_view(), name='all_services'),
    url(r'^category/(?P<cat>[-\w\d]+)/$',
        views.ServicesFromCatList.as_view(), name='cat_service_list'),
    url(r'^services/(?P<country>[-\w\d]+)/$', CitiesView.as_view(),
        name='cities_list'),
    url(r'^services/(?P<country>[-\w\d]+)/(?P<city>[-\w\d]+)/$',
        views.CatsView.as_view(), name='cats_list'),
    url(r'^service/(?P<slug>[-\w\d]+)/$', views.ServiceDetail.as_view(), name='service_detail'),
    url(r'^about-us/$', views.AboutPage.as_view(), name='about_page'),
    url(r'^success/$', views.SuccessPaymentPage.as_view(), name='success_page'),
    url(r'^contacts/$', views.ContactsPage.as_view(), name='contacts_page'),
    url(r'^account/$', views.AccountView.as_view(), name='account_page'),
]
