from geography.models import Country


def get_countries(request):
    items = Country.objects.all().order_by('order')
    return {'countries_footer': items}