from modeltranslation.translator import translator, TranslationOptions
from .models import MainPageConfig, AboutUsPageConfig, GeneralOptions


class MainPageTranslationOptions(TranslationOptions):
    fields = ('title', 'desc')


class AboutUsPageTranslationOptions(TranslationOptions):
    fields = ('title', 'desc', 'full_desc')


class GeneralOptionsTranslationOptions(TranslationOptions):
    fields = ('site_title', 'site_desc', 'footer_desc')


translator.register(MainPageConfig, MainPageTranslationOptions)
translator.register(AboutUsPageConfig, AboutUsPageTranslationOptions)
translator.register(GeneralOptions, GeneralOptionsTranslationOptions)


