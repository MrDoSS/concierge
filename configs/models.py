from django.db import models
from django.utils.translation import ugettext as _
from django.core.cache import cache
from solo.models import SingletonModel
from ckeditor_uploader.fields import RichTextUploadingField
from subprocess import Popen


class AbstractConfig(SingletonModel):
    title = models.CharField(max_length=255, verbose_name=_('Title'), null=True, blank=True)
    desc = models.TextField(verbose_name=_('Description'), null=True, blank=True)

    class Meta:
        abstract = True


class GeneralOptions(SingletonModel):
    site_title = models.CharField(max_length=255, verbose_name=_('Site title'))
    site_desc = models.TextField(verbose_name=_('Site description'))
    footer_desc = models.TextField(verbose_name=_('Description in footer'))


class MainPageConfig(AbstractConfig):
    pass


class AboutUsPageConfig(AbstractConfig):
    full_desc = RichTextUploadingField(verbose_name=_("Full description"), blank=True, null=True)