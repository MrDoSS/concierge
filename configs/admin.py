from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationStackedInline

from engine.models import Feature
from .models import MainPageConfig, AboutUsPageConfig, GeneralOptions
from solo.admin import SingletonModelAdmin


class FeatureInline(TranslationStackedInline):
    model = Feature
    extra = 0


@admin.register(MainPageConfig)
class MainPageConfigAdmin(TranslationAdmin, SingletonModelAdmin):
    list_display = ('title',)
    inlines = (FeatureInline,)

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


@admin.register(AboutUsPageConfig)
class AboutUsConfigAdmin(TranslationAdmin, SingletonModelAdmin):
    list_display = ('title',)

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }


@admin.register(GeneralOptions)
class GeneralConfigAdmin(TranslationAdmin, SingletonModelAdmin):
    list_display = ('id',)

    class Media:
        js = (
            '/static/modeltranslation/js/force_jquery.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js',
            '/static/modeltranslation/js/tabbed_translation_fields.js',
        )
        css = {
            'screen': ('/static/modeltranslation/css/tabbed_translation_fields.css',),
        }